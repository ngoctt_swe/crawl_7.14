import schedule
import pymongo
from scrapy.utils.project import get_project_settings
from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from tutorial.spiders.cve_details import CveDetailsSpider
from tutorial.spiders.cve import CveSpider
from tutorial.spiders.scheduler import SchedulerSpider

def get_cve_len():
    settings = get_project_settings()
    connection = pymongo.MongoClient(
        settings['MONGODB_SERVER'],
        settings['MONGODB_PORT']
    )
    db = connection[settings['MONGODB_DB']]
    collection = db['cves']
    return collection.count_documents({})

if __name__ == '__main__':
    configure_logging()
    runner = CrawlerRunner(get_project_settings())

    @defer.inlineCallbacks
    def crawl():
        yield runner.crawl(CveSpider)
        length = get_cve_len()
        runner.crawl(CveDetailsSpider, inp1=0, inp2=int(length / 4))
        runner.crawl(CveDetailsSpider, inp1=int(length / 4) + 1, inp2=int(length / 2))
        runner.crawl(CveDetailsSpider, inp1=int(length / 2) + 1, inp2=int(3 * length / 4))
        runner.crawl(CveDetailsSpider, inp1=int(3 * length / 4) + 1, inp2=int(length))
        d = runner.join()
        yield d
        yield schedule.every().hour.do(runner.crawl(SchedulerSpider))
        reactor.stop()

    crawl()
    reactor.run()




