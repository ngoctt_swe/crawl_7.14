import pymongo
import logging
from scrapy.utils.project import get_project_settings
from scrapy.exceptions import DropItem

class TutorialPipeline:
    def __init__(self):
        settings = get_project_settings()
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        self.db = connection[settings['MONGODB_DB']]
        self.items = []

    def process_item(self, item, spider):
        valid = True
        for data in item:
            if not data:
                valid = False
                raise DropItem("Missing {0}!".format(data))
        if valid:
            self.items.append(dict(item))
            if len(self.items) >= get_project_settings()['NUMBER_OF_ITEMS']:
                self.insert_current_items(spider)
        return item

    def insert_current_items(self, spider):
        items = self.items
        self.items = []
        try:
            collection = self.db[spider.settings.attributes['COLLECTION_NAME'].value]
            if spider.name == "scheduler":
                for item in items:
                    collection.update({"skybox_id": item["skybox_id"]}, {"$set": item})
            else:
                collection.insert_many(items)
        except:
            logging.info("Item already in database")

    def close_spider(self, spider):
        self.insert_current_items(spider)